﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CalendarWorkSchedule.Models;

namespace CalendarWorkSchedule.Controllers
{
    public class TasksController : Controller
    {
        // GET: Tasks
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetTasks()
        {
            using (ApplicationDbContext dm = new ApplicationDbContext())
            {
                dm.Configuration.LazyLoadingEnabled = false;
                var tasks = dm.Events.ToList();
                return new JsonResult { Data = tasks, JsonRequestBehavior = JsonRequestBehavior.AllowGet };


            }

        }

        public ActionResult Test()
        {
            return View();
        }
    }
}