﻿var tasks = [];
var selectedTask = null;
function FetchTaskAndRenderTasks() {
    tasks = [];
    $.ajax({
        type: "GET",
        url: "/Tasks/GetTasks",
        success: function (data) {
            $.each(data, function (i, v) {
                tasks.push({

                    title: v.Subject,
                    description: v.Description,
                    dueDate: v.Start

                });
            })
            GenerateTasks(tasks);
        },
        error: function (error) {
            console.log(error.responseText);
            alert('failed 1');
        }
    });
}
FetchTaskAndRenderTasks();


function GenerateTasks(tasks) {
    $('#todo-lists-demo1').lobiList({
        lists: [
            {
                title: 'TODO',
                defaultStyle: 'lobilist-info',
                items: tasks
            }
        ]
    });
}


























$(function () {
    $('#todo-lists-demo').lobiList({
        lists: [
            {
                title: 'TODO',
                defaultStyle: 'lobilist-info',
                items: [
                    {
                        title: 'Floor cool cinders',
                        description: 'Thunder fulfilled travellers folly, wading, lake.',
                        dueDate: '2015-01-31'
                    },
                    {
                        title: 'Periods pride',
                        description: 'Accepted was mollis',
                        done: true
                    },
                    {
                        title: 'Flags better burns pigeon',
                        description: 'Rowed cloven frolic thereby, vivamus pining gown intruding strangers prank treacherously darkling.'
                    },
                    {
                        title: 'Accepted was mollis',
                        description: 'Rowed cloven frolic thereby, vivamus pining gown intruding strangers prank treacherously darkling.',
                        dueDate: '2015-02-02'
                    }
                ]
            },
            {
                title: 'DOING',
                items: [
                    {
                        title: 'Composed trays',
                        description: 'Hoary rattle exulting suspendisse elit paradises craft wistful. Bayonets allures prefer traits wrongs flushed. Tent wily matched bold polite slab coinage celerities gales beams.'
                    },
                    {
                        title: 'Chic leafy'
                    },
                    {
                        title: 'Guessed interdum armies chirp writhes most',
                        description: 'Came champlain live leopards twilight whenever warm read wish squirrel rock.',
                        dueDate: '2015-02-04',
                        done: true
                    }
                ]
            }
        ]
    });
});
